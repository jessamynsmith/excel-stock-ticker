AmCharts.useUTC = true;

var dataSet = {
  "color": "#7cb5ec",
    "fieldMappings": [{
      "fromField": "col0",
      "toField": "col0"
    }, {
      "fromField": "col1",
      "toField": "col1"
    }],
    "compared": false,
    "categoryField": "col0",
    "dataLoader": {
      "url": "egin.xls",
      "format": "csv",
      "showCurtain": true,
      "showErrors": true,
      "async": true,
      "delimiter": "\t",
      "skip": 2
    }
};

var chart = AmCharts.makeChart("chartdiv", {
  "type": "stock",
  "theme": "light",
  "dataSets": [dataSet],

  "panels": [{
    "title": "EGIN Stock Price",
    "stockGraphs": [{
      "id": "g1",
      "valueField": "col1",
      "lineThickness": 2,
      "bullet": "round"
    }],
    "stockLegend": {
      "valueTextRegular": " ",
      "markerType": "none"
    }
  }],

  "chartScrollbarSettings": {
    "graph": "g1"
  },

  "chartCursorSettings": {
    "valueBalloonsEnabled": true,
    "graphBulletSize": 1,
    "valueLineBalloonEnabled": true,
    "valueLineEnabled": true,
    "valueLineAlpha": 0.5
  },

  "categoryAxesSettings": {
    "groupToPeriods": ["DD"]
  },

  "valueAxesSettings": {
    "minimum": 0
  },

  "periodSelector": {
    "position": "top",
    "hideOutOfScopePeriods": false,
    "periods": [{
      "period": "MM",
      "count": 1,
      "label": "1 month"
    }, {
      "period": "MM",
      "count": 3,
      "label": "3 months",
      "selected": true
    }, {
      "period": "MM",
      "count": 6,
      "label": "6 months"
    }, {
      "period": "YYYY",
      "count": 1,
      "label": "1 year"
    }, {
      "period": "YTD",
      "label": "YTD"
    }, {
      "period": "MAX",
      "label": "MAX"
    }]
  },

  "panelsSettings": {
    "usePrefixes": true
  },
  "export": {
    "enabled": true
  }
});
