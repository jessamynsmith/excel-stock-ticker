# excel-stock-ticker

Display graph of stock data from a CSV 

## Development

Clone the source from bitbucket:

    git clone git@bitbucket.org:lammworks/excel-stock-ticker.git

Ensure you have node installed, then use npm to install JavaScript dependencies:

    npm install
    
Run the server:

    node server.js
    
View in the browser:

    http://127.0.0.1:5000


## Deployment
    
This project is already set up for deployment to Heroku. Simply create a Heroku app and push the source to Heroku.